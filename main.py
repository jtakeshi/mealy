import sys
import getopt
from MealyMachine import MealyMachine

def main():
  options, remainders = getopt.gnu_getopt(sys.argv[1:], 'f:')
  if len(remainders) == 0:
    print("No input specified, exiting")
    return 0
    
  infile = ''
  for opt, arg in options:
    if opt in ('-f'):
      infile = arg
      
  if infile == '':
    print("ERROR: no infile specified")
    return 0    

  f = open(infile)
  m = MealyMachine(f)
  f.close()
  for arg in remainders:
    print('Input:\t' + arg)
    m.reset()
    out = m.run(list(arg)) #Need to make sure input is in list format
    if out is None:
      print("FAILURE: machine entered fail state for input " + arg)
      continue
    assert('\n' not in out)  
    print('Output:\t' + out + '\n')
    if m.has_final_states():
      print('ACCEPT' if m.is_accepting() else 'REJECT')
    
  return 0  
    

if __name__ == '__main__':
  main()
