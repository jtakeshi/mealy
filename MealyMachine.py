import sys

from State import State

class MealyMachine:
  def __init__(self, infile):
    self.states = dict()
    self.initial = None
    self.finals = dict() #Do we need this?
    self.output = ''
    self.accept = False
    self.current_state = None
    state_lines = [x.rstrip() for x in infile.readlines() if x[0] != '#']
    final_line = [x for x in state_lines if x[0] == '@']
    state_lines = [x for x in state_lines if x[0] != '@']
    if len(final_line) > 1:
      print("ERROR: too many lines with final states specified")
    self.final_states = list()
    if len(final_line) > 0:
      for f in final_line[0].split(' '):
        if f != '@':
          self.final_states.append(f)
    if len(state_lines) == 0:
      print("ERROR: invalid input")
      sys.exit(0)
    for s in state_lines:
      st = State(s)
      self.states[st.label] = st
      if self.initial is None:
        self.initial = st.label
    assert(self.initial is not None)
    self.reset()
    if not self.validate_states():
      print("ERROR: state machine validation failed (invalid transition specified)")
      sys.exit(0)
    self.inputs = self.allowed_input()  
    
  def reset(self):
    self.output = ''
    self.current_state = self.initial
    self.accept = False      
    assert(self.current_state is not None)
    
  def validate_states(self):
    if len(self.states) == 0:
      return False
    for st in self.states:
      to_go = self.states[st].states_to_go_to()
      for t in to_go:
        if t not in self.states:
          return False
    for st in self.final_states:
      if st not in self.states:
        print("ERROR: final state not valid: " + st)
        return False      
    return True         
  
  def allowed_input(self):
    ret = list()
    if len(self.states) == 0:
      return ret
    for st in self.states:
      al = self.states[st].allowed_input()
      for a in al:
        if a not in ret:
          ret.append(a)
    return ret      
    
  def do_one_transition(self, in_val):
    if in_val not in self.inputs:
      return None
    if self.current_state not in self.states:
      print("ERROR: current state " + str(self.current_state) + " not in machine states")  
      return None
    trans_pair = self.states[self.current_state].delta(in_val)
    self.current_state = trans_pair[0]
    self.output += trans_pair[1]
    return ''  
    
  def run(self, in_vals):
    for s in in_vals:
      rval = self.do_one_transition(s)
      if rval is None:
        return None
    return self.output  
    
  def has_final_states(self):
    return True if len(self.final_states) > 0 else False
    
  def is_accepting(self):
    if len(self.final_states) == 0:
      return True
    return True if self.current_state in self.final_states else False    
    
