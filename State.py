import sys

DELIM_DEFAULT=':'

class State:

  #Transitions are in order: input, next state, output
  #Output is optional
  def __init__(self, line, delim=DELIM_DEFAULT):
    parsed_line = line.strip().split(' ')
    self.label = parsed_line[0]
    self.transitions = dict()
    if len(parsed_line) >= 2:
      trans = parsed_line[1:]
      for t in trans:
        parsed_trans = t.split(delim)
        if parsed_trans[0] in self.transitions:
          print("ERROR: input already specified: " + self.label + ", " + parsed_trans)
        if len(parsed_trans) > 3 or len(parsed_trans) < 2:
          print("ERROR: incorrect transition for state " + self.label + ": " + parsed_trans)
          sys.exit(0)
        to_add = (parsed_trans[1], parsed_trans[2] if len(parsed_trans) == 3 else '')
        self.transitions[parsed_trans[0]] = to_add
          
  def delta(self, in_val):
    if in_val not in self.transitions:
      return None
    return self.transitions[in_val]  
          
          
  def allowed_input(self):
    ret = list()
    for i in self.transitions.keys():
      ret.append(i)
    return ret  
    
  def states_to_go_to(self):
    ret = list()
    for tr in self.transitions.values():
      ret.append(tr[0])
    return ret    
      
