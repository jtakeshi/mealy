# Mealy Machine Implementation

This repository implements a Mealy machine. It consists of 3 files:

  -State.py stores a single state, which remembers its own label and a transition function, implemented as a dictionary mapping inputs to pairs of states and outputs. Outputs may be empty.
  
  -MealyMachine.py implements the Mealy machine. It stores a set of states, an initial and current state, and a running output. do\_one\_transition can be used to run the machine one input character/string at a time, while run can be used to run the machine on a list of input characters/strings. reset can be used to reset the machine to an initial state, which will reset the initial and current states, along with the running output.
  
  -main.py is the driver. It requires one state specification file, argued with -f, and will also read an arbitrary number of inputs. As written, the driver will cease operation of the machine on a particular input upon reaching a failure state, and will not print output. (This can be changed by a user.)
  
# File specification  
  
The state machine is specified with a file, where each line specifies a state. (Comments may also be included by beginning a line with '#'.) A line consists of at least one string, whitespace-delimited. The first string will be the state's label. Every string after that should be in the format a:b or a:b:c, where a denotes an input, b is the state to transition to upon that input, and c is the resulting output. If c is not specified, no output will be produced from this transition. The initial state will be the first one specified.

Optionally, a set of final states may be argued. This is done by providing a line starting with '@ ', followed by a whitespace-delimited list of final states. If final states are argued, then is\_accepting can be used to see if the machine is in an accepting state. If no final states are specified, then the machine will always be accepting.

# Some Engineering Considerations
For implementation efficiency, dictionaries hash string labels instead of State objects. This allows less computation and storage in the "hot" operation of looking up state/output pairs during the transition function.

The code is generally modular and extensible; MealyMachine and State are implemented separately. In particular, State could be used in other state machine implementations with only minor modifications, if any.

Dictionaries are used for lookup efficiency with an arbitrary number of states and transitions, but for smaller state sets a list may be more efficient.

A reasonable amount of error checking is provided. Besides formatting checks, the machine's validity is also checked.

Different options are provided for the user to make custom use of the MealyMachine, if so desired. The machine can be run one character at a time, or on the entire input string. Both output and accepting/rejecting are optional. Also, failure can be handled by the calling application (as demonstrated).

The file format is also designed to be easily readable and extensible.